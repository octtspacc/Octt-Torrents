#!/bin/sh

Trackers=https://raw.githubusercontent.com/ngosang/trackerslist/master/trackers_best.txt

#rm -rf ./Octt-Torrents.sitoctt.tmp
mkdir -p ./Octt-Torrents.sitoctt.tmp
cd ./Octt-Torrents.sitoctt.tmp

#rm ./sitoctt.torrent
#cp -r ../sitoctt ./sitoctt
wget https://github.com/octospacc/sitoctt/archive/refs/heads/gh-pages.zip
unzip ./gh-pages.zip

mv ./sitoctt-gh-pages ./sitoctt
#cd ./sitoctt
#sed -i "s/https\:\/\/octtspacc.gitlab.io\/sitoctt-assets\//sitoctt-assets\//g" *.html
#cd ..

#ln -s ../../sitoctt-assets/public ./sitoctt/sitoctt-assets
mktorrent \
	$(for i in $(curl $Trackers | grep "://"); do echo "-a $i"; done) \
	-w https://octtspacc.gitlab.io/ \
	./sitoctt/
#rm ./sitoctt

Hash=$(transmission-show sitoctt.torrent | grep "Hash:" | awk '{print $2}')
mv ./sitoctt.torrent ./$Hash.torrent

cat > feed.rss << [EOF]
<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0">
	<channel>
		<title>sitoctt</title>
		<link>https://sitoctt.octt.eu.org</link>
		<item>
			<title>sitoctt</title>
			<guid>$Hash</guid>
			<enclosure
				url="https://octtspacc.gitlab.io/Octt-Torrents/sitoctt/$Hash.torrent"
				type="application/x-bittorrent" />
		</item>
	</channel>
</rss>
[EOF]

mkdir -p ../Octt-Torrents/public/sitoctt
cp ./feed.rss ./$Hash.torrent ../Octt-Torrents/public/sitoctt/
